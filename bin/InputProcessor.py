# v1.01

# This file is part of TGBOT.

# TGBOT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# TGBOT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with TGBOT.  If not, see <http://www.gnu.org/licenses/>.

# Local imports
# Imports Logger class as well as predefined Logging levels(INFO, DEBUG, ERROR)
from logger import *
from TGInterface import TGInterface
from config import Config
from botcalendar import BotCalendar
from MessageQueue import MessageQueue
from DBHandler import DBHandler


import json
import re
import random
from datetime import datetime, timedelta, timezone
import pytz
from collections import OrderedDict
import time

# ###################################
#  InputProcessor
#
#  Processes the incoming text from
#  Telegram and other clients to
#  determine if cation needs to be
#  taken by the bot.
# ###################################
class InputProcessor:
        
    def __init__(self):    
        self.logger = Logger()
        self.TGI = TGInterface()
        self.config = Config()
#        self.Poller = Poller()
        self.CAL = BotCalendar()
        self.MQ = MessageQueue()
        self.DB = DBHandler()
        self.ZONE = -6

    # ###################################
    #  Log
    #
    #  Local log method to specify the
    #  name of the class/file of the
    #  caller.
    # ###################################
    def log(self, level, statement):
        self.logger.log(level, "InputProcessor -- {}".format(statement))

    # ###################################
    #  new_member_check
    #
    #  Checks to see if a new member has
    #  joined the chat. If so, triggers a
    #  welcome message to be sent based on
    #  the one defined in the config file.
    # ###################################
    def new_member_check(self, message_json):
        self.log(DEBUG, "func --> new_member_check")
        mType = self.getMType(message_json)
        if 'new_chat_member' in message_json[mType]:
            self.TGI.welcome_say(message_json[mType]["new_chat_member"]["first_name"])

    # ###################################
    #  getMType
    #
    #  Determines the type of message that
    #  has been sent to the bot and sets
    #  a variable declaring said type.
    # ###################################
    def getMType(self, message_json):
        self.log(DEBUG, "func --> getMType")
        if 'callback_query' in message_json:
            mType = 'callback_query'
        elif "message" in message_json:
            mType = "message"
        elif "edited_message" in message_json:
            mType = "edited_message"
        elif "inline_query" in message_json:  # Not fully supported at this moment.
            mType = "inline_query"
        else:
            mType = "other"
        return mType

    def addPeak(self, update_time, message_id, peak):
        self.log(DEBUG, "addPeak({}, {}, {})".format(update_time, message_id, peak))
        update_time = datetime.utcfromtimestamp(int(update_time)).replace(tzinfo=timezone.utc).astimezone(tz=None)
        hour = int(re.sub(r'^0', r'', update_time.strftime('%H')))
        self.log(DEBUG, "Peak: {}, Hour: {}, type(peak): {}".format(peak, hour, type(peak)))
        peak[hour][str(message_id)] = update_time.strftime("%Y/%m/%d %H:%M:%S")
        return peak

    def addUsers(self, user_id, update_time, avgusers):
        self.log(DEBUG, "addUsers({}, {}, {})".format(user_id, update_time, avgusers))
        update_time = datetime.utcfromtimestamp(int(update_time)).replace(tzinfo=timezone.utc).astimezone(tz=None)
        hour = int(re.sub(r'^0', r'', update_time.strftime('%H')))
        avgusers[hour][str(user_id)] = update_time.strftime("%Y/%m/%d %H:%M:%S")
        return avgusers 

    def recordAnalytics(self, post_data):
        self.log(DEBUG, "recordAnalytics({})".format(post_data))
        analytics = self.DB.retrieveAnalytics()
        self.log(DEBUG, "Analytics: {}".format(analytics))
        peak = json.loads(analytics[1])
        self.log(DEBUG, "peak: {}, Type: {}".format(peak, type(peak)))
        avgusers = json.loads(analytics[2])

        mType = self.getMType(post_data)
        update_time = post_data[mType]['date']
        user_id = post_data[mType]['from']['id']
        message_id = post_data[mType]['message_id']
        data = {
                "peak": self.addPeak(update_time, message_id, peak),
                "avgusers": self.addUsers(user_id, update_time, avgusers), 
                }

        self.DB.recordAnalytics(data)

    def buildPeakString(self, peak):
        peak_string = """
The following is in military time...

Hour | # of Messages
--------------------
{}
        """
        data = ""
        i = 0
        for index in peak:
            if i < 10:
                hour = "0{}".format(i)
            else: 
                hour = "{}".format(i)

            data = data + "{}   | {}\n".format(hour, len(index))
            i = i + 1
           
        return peak_string.format(data)

    def buildAVGUsersString(self, avgusers):
        avgusers_string = """
The following is in military time...

Hour | # of Unique Users
------------------------
{}
        """
        data = ""
        i = 0
        for index in avgusers:
            if i < 10:
                hour = "0{}".format(i)
            else:
                hour = "{}".format(i)

            data = data + "{}   | {}\n".format(hour, len(index))
            i = i + 1

        return avgusers_string.format(data)

    def buildRestartString(self, restarts):
        #TODO Remember to wipe out data that's more than 24 hours old!

        restarts_string = """
The following is in military time...

Date of restarts 
------------------------
{}
        """
        data = ""
        for index in restarts:
            data = data + "{}\n".format(index)
            
        return restarts_string.format(data)

    def getAnalytics(self):
        self.log(DEBUG, "getAnalytics()")
        analytics = self.DB.retrieveAnalytics()
        self.log(DEBUG, "Analytics: {}".format(analytics))
        peak = json.loads(analytics[1])
        while type(peak) == 'str':
            peak = json.loads(peak)
        peak = self.buildPeakString(peak)
        avgusers = json.loads(analytics[2])
        while type(avgusers) == 'str':
            avgusers = json.loads(avgusers)
        avgusers = self.buildAVGUsersString(avgusers)
        restarts = json.loads(analytics[3])
        restarts = self.buildRestartString(restarts)
        output = """
Peak:
{}

Users:
{}

Restarts:
{}
        """.format(peak, avgusers, restarts)
        return output

    def getHelp(self):
        self.log(DEBUG, "getHelp()")
        output = """
Help:

Enter any of the following commands:

Help or help:
    This command will print this help message

Analytics or analytics:
    Will display output pertaining to information about the group. This includes: Peak Messaging Hours, Unique users per hour, and the dates and times the bot has restarted during the day. 

    Note: This information pertains to ONLY the past 24 hours. This may get extended at a later date.

last active or Last Active:
    This will display how long it has been since the user last spoke in chat.
    Optionally: Include a number after, e.g. 'last active 4' to get all users who's last comment is older than that number of days. 

add calendar or Add Calendar <PUBLIC URL TO CALENDAR>:
    Adds a calendar to the bot's list of active calendars. The calendar must be publicly available.
        """
        return output

    def getLastActive(self, text):
        self.log(DEBUG, "getLastActive()")
        textArr = text.split()
        if len(textArr) > 2:
            numDays = float(textArr[2])
        else:
            numDays = -1
        lastactive = self.DB.retrieveLastActive()
        lastactive = json.loads(lastactive[1])
        self.log(DEBUG, "lastactive: {}, Type: {}".format(lastactive, type(lastactive)))
        la_sorted = OrderedDict(sorted(lastactive.items(), key=lambda item: item[1]["date"]))
        self.log(DEBUG, "la_sorted = {}".format(la_sorted))

        output = '''
Last Active Stats:

Name - Username - lastactive
----------------------------
{}
        '''
        tz_CDT = pytz.timezone('America/Chicago')
        data = ''
        for key, value in la_sorted.items():

            comment_date = datetime.utcfromtimestamp(value['date']).replace(tzinfo=timezone.utc).astimezone(tz=tz_CDT)
            now = datetime.now(tz=tz_CDT)
            difference = now - comment_date
            seconds_in_day = 24 * 60 * 60
            duration_in_s = difference.total_seconds()
            days = int(duration_in_s)/seconds_in_day
            if numDays == -1 or days > numDays:
                time_string = "{} Day(s) since last post".format(round(days, 2))
                data = data + "{} - {} - {} at {}\n\n".format(value['name'], value['@'], time_string, comment_date)

        return output.format(data)

    def recordLastActive(self, post_data):
        self.log(DEBUG, "recordLastActive({})".format(post_data))
        mType = self.getMType(post_data)
        user_id = post_data[mType]['from']['id']
        name = post_data[mType]['from']['first_name'] 
        if 'last_name' in post_data[mType]['from']:
            name = name + " " + post_data[mType]['from']['last_name']
        if 'username' in post_data[mType]['from']:
            username = "@" + post_data[mType]['from']['username']
        else:
            username = ''
        date = post_data[mType]['date']
        data = {
                'name': name,
                '@': username,
                'date': date,
                'user_id': user_id
                }
        self.DB.recordLastActive(data)

    def addCalendar(self, text):
        self.log(DEBUG, "addCalendar()")
        calID = text[13:]
        calAdded = self.CAL.addCalendar(calID)
        if calAdded:
            output = "Calendar Added Successfully!"
        else:
            output = "Calendar NOT ADDED :("
        return output

    def process_conversation(self, post_data):
        self.log(DEBUG, "process_conversation({})".format(post_data))
        mType = self.getMType(post_data)
        chat_id = post_data[mType]['chat']['id']
        text = post_data[mType]['text']

        # Add calendar
        # Get Help
        if re.match('[Aa]nalytics', text) is not None:
            output = self.getAnalytics()
        elif re.match('[Hh]elp', text) is not None:
            output = self.getHelp()
        elif re.match('[Ll]ast [Aa]ctive', text) is not None:
            output = self.getLastActive(text)
        elif re.match('/[Ss]tart', text) is not None:
            output = self.getHelp()
        elif re.match('[Aa]dd [Cc]alendar .+', text) is not None:
            output = self.addCalendar(text)
        else:
            output = "Command Not Found..."
        
        if len(output) > 4096:
            outputA = output[:4095]
            outputB = output[4096:]
            self.TGI.bot_say(outputA, chat_id=chat_id, code=True)
            time.sleep(1)
            self.TGI.bot_say(outputB, chat_id=chat_id, code=True)
        else:
            self.TGI.bot_say(output, chat_id=chat_id, code=True)

    # ################################################
    #  process_post_data
    #
    #  Takes the post_data for an incoming
    #  message and sends it to process_now
    #  to check if it needs to be processed now.
    #  The return response or message from Telegram is
    #  added to the queue to be processed_later. Following
    #  that the original message is run through the later
    #  processor to be logged.
    # ################################################
    def process_post_data(self, post_data):
        self.log(DEBUG, "process_post_data({})".format(post_data))
        post_data = json.loads(post_data.decode('utf-8'))
        try:
            chat_id = post_data[self.getMType(post_data)]['chat']['id']
        except KeyError:
            chat_id = int(self.config.telegram['CHAT_RESTRICTION'])
        

        self.log(DEBUG, "chat_id: {}, chat_restriction: {}".format(chat_id, self.config.telegram['CHAT_RESTRICTION']))
        if chat_id == int(self.config.telegram['CHAT_RESTRICTION']):
            if "botsay" not in post_data:
                self.recordAnalytics(post_data)
                self.recordLastActive(post_data)
            response = self.process_now(post_data)
            if response is not None:
                self.log(DEBUG, "RESPONSE: {}".format(response))
                if self.process_later(response):
                    self.MQ.addMessage(response)
            if self.process_later(post_data):
                mType = self.getMType(post_data)
                if mType != 'other' and 'text' in post_data[mType]:
                    text = post_data[mType]['text']
                    keep_re = re.compile("#{}".format(self.config.group_info['KEEP']))
                    if keep_re.search(text) is None:
                        self.MQ.addMessage(post_data)
                elif mType != 'other' and 'text' not in post_data[mType]:
                    self.MQ.addMessage(post_data)
        elif self.admincheck(post_data):
            self.process_conversation(post_data)
        else:
            self.log(ERROR, "Message not recorded: {}".format(post_data))


    def admincheck(self, post_data):
        admins = self.TGI.get_admin_json()
        chat_user_id = post_data[self.getMType(post_data)]['from']['id']
        verified = False
        for admin in admins:
            if admin['user']['id'] == chat_user_id:
                verified = True
        return verified

    # ###################################
    #  process_later
    #
    #  Determines if a message will need
    #  processing later. In the current
    #  case, if a message will need to be
    #  deleted at a later date.
    # ###################################
    def process_later(self, post_data):
        self.log(DEBUG, "func --> process_later")
        if "edited_message" in post_data or "message" in post_data:
            return True
        else:
            return False

    def event_request_check(self, text):
        self.log(DEBUG, "func -> event_request_check")
        if re.compile(self.config.telegram["BOTNAME"]).match(text) is not None and re.compile(self.config.RESPONSES['events'][0]).search(text) is not None:
            self.log(DEBUG, "EVENTLIST AND BOTNAME MATCH: {}".format(text))
            self.CAL.check()
            return True
        return False

    def summon_check(self, text):
        self.log(DEBUG, "func -> summon_check")
        if re.compile(self.config.telegram["BOTNAME"]).search(text) is not None and re.compile(self.config.RESPONSES['summonadmins'][0]).search(text) is not None:
            self.log(DEBUG, "SUMMON AND BOTNAME MATCH: {}".format(text))
            response = self.TGI.summon_admins()
            self.MQ.addMessage(response)
            return True
        else:
            self.log(DEBUG, "Summon and Botname do NOT match!! {}".format(text))
        return False

#    def poll_request_check(self, text):
#        self.log(DEBUG, "func -> poll_request_check")
#        if re.compile(self.config.telegram["BOTNAME"]).match(text) is not None and re.compile(self.config.RESPONSES['newpoll'][0]).search(text) is not None:
#            self.log(DEBUG, "NEWPOLL AND BOTNAME MATCH: {}".format(text))
#            self.Poller.newPoll(text)
#            self.log(DEBUG, "Poll created with {}".format(text))
#            return True
#        return False

#    def pass_to_poller(self, message):
#        self.log(DEBUG, "func -> pass_to_poller")
#        self.Poller.callback_handler(message)


    def get_timeout(self, key):
        self.log(DEBUG, "func -> get_timeout")
        if not key in self.config.TIMEOUT:
            timeout = 0
        else:
            timeout = self.config.TIMEOUT[key]
        return timeout

    def check_timeout(self, timeout):
        self.log(DEBUG, "func -> check_timeout")
        now = datetime.today().replace(microsecond=0,tzinfo=timezone(timedelta(hours=self.ZONE)))

        if timeout == 0 or timeout <= now:
            self.log(DEBUG, "TIMEOUT NOT IN EFFECT!!!")
            return True
        self.log(DEBUG, "TIMEOUT STILL IN EFFECT...")
        return False

    def set_timeout(self, timeout, key):
        self.log(DEBUG, "func -> set_timeout")
        now = datetime.today().replace(microsecond=0,tzinfo=timezone(timedelta(hours=self.ZONE)))
        new_timeout = now + timedelta(seconds=+int(self.config.telegram["BOTLIMIT"]))
        self.config.TIMEOUT[key] = new_timeout
        self.log(DEBUG, "Set Done: {}".format(self.config.TIMEOUT[key]))

    def process_triggers(self, text):
        self.log(DEBUG, "func --> process_triggers")
        

        if not self.event_request_check(text) and not self.summon_check(text): # and not self.poll_request_check(text):
            for key, value in self.config.triggers["TRIGGERS"].items():
                timeout = self.get_timeout(key)
                try:
                    value = value.format(self.config.telegram["BOTNAME"])
                    self.log(DEBUG, "FILLED IN THE BOTNAME. TRigger is now [{}]".format(value))
                except IndexError:
                    self.log(DEBUG, "No need to fill in the botname. This trigger [{}] doesn't take a name.".format(value)) 
        

                if self.check_timeout(timeout):
                    re_value = re.compile(value)
                    if re_value.search(text) is not None:
                        self.log(DEBUG, "SETTING THE TIMEOUT")
                        self.set_timeout(timeout, key)
                        self.MQ.addBotMessage(random.choice(self.config.RESPONSES[key]))
                        #self.TGI.testKeyboard()
                        break;
                    else:
                        self.log(DEBUG, "No Match for [{}] in text [{}]".format(value, text))



    # ###################################
    #  process_now
    #
    #  Determines if a message needs to be
    #  processed now. In this case, if
    #  someone has joined chat, a botsay
    #  event has been sent, or an eventlist
    #  has been requested.
    # ###################################
    def process_now(self, message):
        self.log(DEBUG, "func --> process_now({})".format(message))

        mType = self.getMType(message)
        response = None
        self.log(DEBUG, "Message: {}".format(message))
        if mType != 'other' and 'text' in message[mType]:
            text = message[mType]['text']
        if "botsay" in message:
            #response = self.TGI.bot_say(message["botsay"])
            response = self.MQ.addBotMessage(message["botsay"])
        elif "left_chat_member" in message:
            self.DB.removeLAMember(message['left_chat_member']['user']['id'])
        elif "bottest" in message:
            self.log(INFO, "SELF TEST TRIGGERED!!!")
            self.config.setActive()
        elif 'new_chat_member' in message[mType] and message[mType]["new_chat_member"]['is_bot'] is False:
            response = self.TGI.welcome_say(message[mType]['new_chat_member']["first_name"])        
#        elif 'callback_query' in message:
#            self.pass_to_poller(message)
        elif 'text' in message[mType]:
            self.process_triggers(text)


        self.log(DEBUG, "response: {}".format(response))
        return response
