#!/usr/bin/env python3

import sys
from os import path
sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))
from botcalendar import BotCalendar
from config import Config

print ('Number of arguments:', len(sys.argv), 'arguments.')
print ('Argument List:', sys.argv)

configName = sys.argv[1]

config = Config(configName)
#CAL = BotCalendar(config.calendar["API_KEY"], config.calendar["CALID"])
CAL = BotCalendar()

print(' CHECK '.center(80, '+'))
print(CAL.check(auto=True))

print('', end='\n\n')

print(' get_urgentEvents '.center(80, '+'))
print(CAL.get_urgentEvents())
