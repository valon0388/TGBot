from logger import *
from Poll import Poll
from DBHandler import DBHandler
import re, json
from string import ascii_uppercase as UPPERS
import collections

class Poller:
    Polls = {}

    def __init__(self):
        self.logger = Logger()
        self.log(INFO, "INIT")
        self.DB = DBHandler()
        self.ZONE = -5

    def newPoll(self, pollString):
        self.log(INFO, "func -> newPoll")
        opts = self.parsePollString(pollString)
        self.log(DEBUG, "opts: {}".format(opts))
        try:
            newPID = self.getNewPollID()
            poll = Poll(opts[0], opts[1], opts[2], newPID)
            self.Polls[newPID] = poll
            #self.showOpenPolls()
            self.showPoll(newPID)
        except Exception as e:
            self.log(ERROR, e)
        return poll

    def log(self, level, statement):
        self.logger.log(level, "Poller -- {}".format(statement))

    def getNewPollID(self):
        self.log(INFO, "func -> getNewPollID")
        return self.DB.getNewPollID()

    def savePolls(self):
        self.log(INFO, "func -> savePolls")

    def loadPolls(self):
        self.log(INFO, "func -> loadPolls")

    def closechecker():
        now = datetime.today()
        currentMinute = now.replace(second=0, microsecond=0, tzinfo=timezone(timedelta(hours=self.ZONE)))
        for key,value in self.Polls.items():
            if value.end_date <= currentMinute:
                value.closePoll()
                value.displayPoll()
                value.showResults()
            

    def callback_handler(self, message):
        self.log(INFO, "func -> callback_handler")
        self.log(DEBUG, message)
        try:
            callback_string = message['callback_query']['data']
            voterID = message['callback_query']['from']['id']
            callback = json.loads(callback_string)
            data = callback['data']
            upid = callback['upid']
            self.Polls[upid].registerVote(data, voterID)
        except Exception as e:
            self.log(ERROR, e)

    def parsePollString(self,text):
        self.log(INFO, "func -> parsePollString")
        self.log(INFO, "QUERY")
        query_re = re.compile('Query:.*$')
        options_re = {letter:re.compile('{}:.*$'.format(letter)) for letter in UPPERS}
        end_date_re = re.compile('End:.*$')
    
        try:
            for line in text.split("\n"):
                query_match = query_re.search(line)
                query_line = ''
                if query_match is not None:
                    self.log(DEBUG, "Query Match: {}".format(query_match))
                    query_line = query_match.string
                    self.log(DEBUG, query_line)
                    query_line = re.sub('Query: ', '', query_line)
                    self.log(DEBUG, query_line)
                    break
                else:
                    pass # TODO Log Error and return Fail
        except Exception as e:
            self.log(ERROR, e)
        
        self.log(INFO, "OPTIONS")
        options = collections.OrderedDict()

        for line in text.split("\n"):
            for key, value in options_re.items():
                self.log(DEBUG, "options_re -- KEY: {} VALUE:{}".format(key, value))
                value_match = value.search(line)
                if value_match is not None:
                    self.log(INFO, "MATCH FOUND IN {}!!!".format(line))
                    #value_line = value_match.group(1)
                    value_line = re.sub('{}:'.format(key), '', line)
                    options[key] = value_line
                #else:
                #    break
        # TODO if no options the return fail
        
        
        self.log(INFO, "ENDDATE")
            
        for line in text.split("\n"):
            end_date_match = end_date_re.search(line)
            end_date_line = ''
            if end_date_match is not None:
                self.log(INFO, "Match for end date found")
                try:
                    end_date_line = datetime.datetime.strptime(re.sub('End:', '', line), "%y-%m-%d--%H:%M")
                except Exception as e:
                    self.log(ERROR, e)
                self.log(INFO, end_date_line)
            else:
                pass  # TODO Log Error and return Fail

        self.log(DEBUG, 'return: {}'.format((query_line, options, end_date_line)))
        return (query_line, options, end_date_line)


    def showOpenPolls(self):
        self.log(INFO, "func -> showOpenPolls")
        for key,poll in self.Polls.items():
            poll.displayPoll()

    def showPoll(self, UPID):
        self.log(INFO, "func -> showPoll")
        for key,poll in self.Polls.items():
            if poll.upid == UPID:
                poll.displayPoll()
                break
