from logger import *
from TGInterface import TGInterface
from DBHandler import DBHandler

import json
import collections
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler

class Poll:
    TGI = TGInterface()

    def __init__(self,query, options, end_date, upid):
        self.logger = Logger()
        self.log(INFO, "Poll INIT")
        self.DB = DBHandler()
        self.query = query
        self.options = options
        self.end_date = end_date
        self.upid = upid
        self.log(INFO, "INIT")
        self.votes = []
        #self.vote = {  
        #               "voterID": voterID,
        #               "vote": vote
        #            }

    def log(self, level, statement):
        self.logger.log(level, "Poll -- {}".format(statement))

    def closePoll(self):
        self.log(INFO, "func -> closePoll")
        self.DB.closePoll(self.upid)

    def registerVote(self, data, voterID):
        self.log(INFO, "func -> registerVote")
        if self.alreadyVoted(voterID):
            self.changeVote(voterID, data)
        else:
            self.setVote(voterID, data)

    def changeVote(self, voterID, vote):
        self.log(INFO, "func -> changeVote")
        for i in self.votes:
            if i['voterID'] is voterID:
                i['vote'] = vote        

    def alreadyVoted(self, voterID):
        self.log(INFO, "func -> alreadyVoted")
        for i in self.votes:
            if i['voterID'] == voterID:
                return True
        return False

    def setVote(self, voterID, vote):
        self.log(INFO, "func -> setVote")
        self.votes.append({"voterID": voterID, "vote":vote})

    def testKeyboard(self):
        self.log(INFO, "func -> testKeyboard")
        keyboard = [[
            InlineKeyboardButton("Option 1", callback_data='1'),
            InlineKeyboardButton("Option 2", callback_data='2'),
            InlineKeyboardButton("Option 3", callback_data='3')
        ]]

        reply_markup = InlineKeyboardMarkup(keyboard)

        reply_markup = json.dumps(reply_markup.to_dict())
        self.bot_say("Options", reply_markup=reply_markup)

    def displayPoll(self):
        self.log(INFO, "func -> displayPoll")
        keyboard = []
        self.log(DEBUG, "Options: {}".format(self.options))
        for key, value in self.options.items():
            keyboard.append([InlineKeyboardButton('{}: {}'.format(key, value),
                                                  callback_data=json.dumps({'upid' : self.upid, 'data' : key}))])
        reply_markup = InlineKeyboardMarkup(keyboard)
        reply_markup = json.dumps(reply_markup.to_dict())
        self.TGI.bot_say(self.query, reply_markup=reply_markup)

    def to_json(self):
        self.log(INFO, "func -> to_json")
        return {'upid': self.upid, 
                "query": self.query,
                "votes": self.votes,
                "options": self.options,
                "end_date": self.end_date
                }
    
    def showResults(self):
        pass
