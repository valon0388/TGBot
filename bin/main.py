#!/usr/bin/env python3

from config import Config
from logger import *
from BRHandler import BRHandler
from TGInterface import TGInterface
from MessageQueue import MessageQueue
from InputProcessor import InputProcessor
from botcalendar import BotCalendar

import multiprocessing
from multiprocessing import Process
import gunicorn.app.base
import _thread
import time
import threading
from threading import Thread
from datetime import datetime, timezone
import sys

config = ''
logger = ''
MQ = ''
DB = ''
CAL = ''
TGI = ''

def number_of_workers():
    return 1 # (multiprocessing.cpu_count() * 2) + 1


def log(level, statement):
    logger.log(level, "main -- {}".format(statement))


class StandaloneApplication(gunicorn.app.base.BaseApplication):

    def __init__(self, app, options=None):
        self.options = options or {}
        self.application = app
        super().__init__()

    def load_config(self):
        config = {key: value for key, value in self.options.items()
                  if key in self.cfg.settings and value is not None}
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application

def __init__(CONFIGFILE):
    global config
    global logger
    global MQ
    global DB
    global CAL
    global TGI

    config = Config(CONFIGFILE)
    logger = Logger(auto=True, logname=config.server['LOGNAME'])
    logger.setup(config.server['LOGNAME'])
    log(DEBUG, "func --> __init__")
    log(DEBUG, "Config file: {}".format(CONFIGFILE))
    TGI = TGInterface()
    CAL = BotCalendar()
    MQ = MessageQueue()
    MQ.loadDB()
    DB = MQ.DB
    DB.recordRestart()
    server_address = { 
            "ip": config.server["IP"], 
            "port": int(config.server["PORT"])
            }
    return server_address

# ###################################
#  calendar_checker
#
#  Starts the loop that triggers the
#  calendar check once every 24 hours.
# ###################################
def calendar_checker(threadName, delay):
    log(INFO, "Starting the calendar_checker thread: {}".format(threadName))
    while True:
        try:
            time.sleep(86400)  # 24 hours
            CAL.check(auto=True)
        except:
            log(ERROR, "Unhandled Exception: {}".format(sys.exc_info()))

def main_loop(MQ, CAl):
    day = 0

    while True:
        try:
            MQ.processQueue()
            CAL.get_urgentEvents()
            dt = datetime.utcnow().replace(tzinfo=timezone.utc).astimezone(tz=None)
            newDay = dt.strftime("%d")
            if day == 0:
                day = newDay
            elif day != newDay:
                log(DEBUG, "It's Midnight! Clearing the Analytics table!")
                day = newDay
                DB.reinitializeAnalytics()
            time.sleep(60)
        except:
            log(ERROR, "Unhandled Exception: {}".format(sys.exc_info()))

if __name__ == '__main__':
    ipPort=__init__(sys.argv[1])
    log(DEBUG, "Printing Startup Message: {}".format(config.group_info['STARTUP']))
    MQ.addBotMessage(config.group_info['STARTUP'])
    handler_app = BRHandler().get_bot()
    options = {
        'bind': '%s:%s' % (ipPort['ip'], ipPort['port']),
        'workers': number_of_workers(),
        'timeout': 120,
        'keyfile': config.server['KEY'],
        'certfile': config.server['CERT'],
        'ssl_version': 'TLS_SERVER', 
        'access-logfile': "{}.log".format(config.server['LOGNAME']),
        'error-logfile': "{}.log".format(config.server['LOGNAME']),
        'log-level': 'debug'
    }
    log(DEBUG, "Options: {}".format(options))
    push_listener = Process(target=StandaloneApplication(handler_app, options).run)
    push_listener.start()
    _thread.start_new_thread(calendar_checker, (("calendar_checker", 0)))
 
    CAL.build_eventQueue()

    log(DEBUG, "AFTER PUSH LISTENER")

    main_thread = Thread(target=main_loop, name="main_loop", args=(MQ, CAL))
    main_thread.start()

    while True:
        if not main_thread.is_alive():
            log(ERROR, "Main Thread is dead. Restarting!")
            main_thread = Thread(target=main_loop, name="main_loop", args=(MQ, CAL))
            main_thread.start()
        else:
            log(DEBUG, "Main Thread is ALIVE!")
        time.sleep(60)
