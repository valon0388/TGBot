# v1.01

# This file is part of TGBOT.

# TGBOT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# TGBOT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with TGBOT.  If not, see <http://www.gnu.org/licenses/>.

# Local imports
# Imports Logger class as well as predefined Logging levels(INFO, DEBUG, ERROR)
from logger import *
from config import Config

import json
import re
import random
import os
import getpass
import urllib
import urllib.parse
import pwd
import requests

# ###################################
#  InputProcessor
#
#  Processes the incoming text from
#  Telegram and other clients to
#  determine if cation needs to be
#  taken by the bot.
# ###################################
class SMSProcessor:
        
    def __init__(self, configfile):    
        self.logger = Logger()
        self.config = Config()
#        self.Poller = Poller()

    # ###################################
    #  Log
    #
    #  Local log method to specify the
    #  name of the class/file of the
    #  caller.
    # ###################################
    def log(self, level, statement):
        self.logger.log(level, "SMSProcessor -- {}".format(statement))

    def process_post_data(self, post_data):
        self.log(DEBUG, "func --> process_post_data({})".format(post_data))
        post_data_json = urllib.parse.parse_qs(post_data.decode("utf-8"))
        self.log(DEBUG, "post_data_json: {}".format(post_data_json))
        body = post_data_json['Body'][0]
        self.log(DEBUG, "BODY: {}".format(body))
        if not self.relayTestCheck(body):
            self.process_now(body)
        self.forward(post_data)
    
    def relayTestCheck(self, body):
        self.log(DEBUG, "func --> relayTestCheck({})".format(body))
        if "#relaytest" in body:
            return True
        return False

    def forward(self, post_data):
        self.log(DEBUG, "func --> forward")
        ips = self.config.server['FORWARD']
        token = self.config.server['TWILIOTOKEN']
        port = self.config.server["PORT"]
        for ip in ips:
            URL = "https://{}:{}/{}".format(ip, port, token)
            self.log(DEBUG, "Shipping text to {}  -- {}".format(URL, post_data))
            requests.post(url = URL, data = post_data, verify=False)

    def process_now(self, message):
        self.log(DEBUG, "func --> process_now")
        self.bot_say(message)

    def bot_say(self, message):
        self.log(DEBUG, "func --> bot_say")
        try:
            pwd.getpwnam(self.config.server['TESTUSER'])
            testUser = self.config.server['TESTUSER']
        except KeyError:
            self.log( INFO, 'User {} does not exist.'.format(self.config.server['TESTUSER']))
            testUser = None
        
 
        dirname = os.path.expanduser('~')
        message = urllib.parse.quote(message)
        self.log(DEBUG, "ENCODED MESSAGE: {}".format(message))
        command = 'echo "{}" | {}/bin/Utils/botpipe {}'
        if eval(self.config.server['LIVE']):
            self.log(DEBUG, "Server is live. Forwarding message to all bots on server...")
            bots = self.getbots()
            self.log(DEBUG, "Found the following Bots: {}".format(bots))
            dirname = os.path.expanduser('/home/groupbot')
            for bot in bots:
                if bot != '':
                    command_f = command.format(message, dirname, bot)
                    self.log(DEBUG, "COMMAND: {}".format(command_f))
                    os.system(command_f)
            if testUser is not None:
                command_f = command.format(message, os.path.expanduser('~'), testUser)
                self.log(DEBUG, "COMMAND: {}".format(command_f))
                os.system(command_f)
                
        elif testUser is not None:
            self.log(DEBUG, "Server is not live.....testing on {}".format(testUser))
            command_f = command.format(message, dirname, testUser)
            self.log(DEBUG, "Command: {}".format(command_f))
            output = os.system(command_f)

    def getbots(self):
        bots = os.popen('cd /home/groupbot; ls *bot.conf | grep -v ^bot.conf | sed "s/.conf//g"').read().split('\n')
        return bots
        
