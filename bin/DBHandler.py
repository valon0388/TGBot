# v1.01

# This file is part of TGBOT.

# TGBOT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# TGBOT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with TGBOT.  If not, see <http://www.gnu.org/licenses/>.

# Local imports
# Imports Logger class as well as predefined Logging levels(INFO, DEBUG, ERROR)
from logger import *  
from config import Config

import sqlite3
import json
import time
import os
from datetime import datetime


# ###################################
#  DBHandler
#
#  Interface to the sqlite3 database
#  that acts as the long term storage
#  for the messages.
# ###################################
class DBHandler:

    # ###################################
    #  __init__
    #
    #  Runs the command to create the
    #  database.
    # ###################################
    def __init__(self):
        self.logger = Logger()
        self.config = Config()
        self.createDB()
        #self.recordRestart(datetime.now())

    def reinitializeAnalytics(self):
        self.log(DEBUG, "reinitializeAnalytics({})".format(self.config.server["DB"]))
        conn = sqlite3.connect(self.config.server["DB"])
        c = conn.cursor()

        # one empty json for each hour of the day, messages are stored as follows: peak[military_hour][message_id] = date
        peak = [{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}]
        # one empty json for each hour of the date, user entries are stored as follows: avgusers[military_hour][user_id] = date
        avgusers = [{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}]
        # one date per array indices
        restarts = []
        
        c.execute("Update analytics SET PEAK = ?, AVGUSERS = ?, RESTARTS = ? WHERE ID = 1", (json.dumps(peak), json.dumps(avgusers), json.dumps(restarts)))
        conn.commit()

        conn.close()

    # ###################################
    #  Log
    #
    #  Local log method to specify the
    #  name of the class/file of the
    #  caller.
    # ###################################
    def log(self, level, statement):
        self.logger.log(level, "DBHandler -- {}".format(statement))

    # ###################################
    #  createDB
    #
    #  If a local database file with the
    #  name specified in the config file
    #  does not exist, it is created and
    #  initialized with a table to store
    #  all the messages for the chat.
    # ###################################
    def createDB(self):
        self.log(DEBUG, "createDB -> {}".format(self.config.server["DB"]))
        conn = sqlite3.connect(self.config.server["DB"])
        c = conn.cursor()
        c.execute("CREATE TABLE IF NOT EXISTS messages(ID integer PRIMARY KEY, DATE text, JSON text);")
        c.execute("CREATE TABLE IF NOT EXISTS polls(ID integer PRIMARY KEY, JSON text, CLOSED bit);")
        c.execute("CREATE TABLE IF NOT EXISTS analytics(ID integer PRIMARY KEY, PEAK text, AVGUSERS text, RESTARTS text)")
        c.execute("CREATE TABLE IF NOT EXISTS lastactive(ID integer PRIMARY KEY, LASTACTIVE text)")
        conn.commit()

        # Check to see if analytics has been populated yet.
        c.execute("SELECT MAX(ID) FROM analytics")
        rows = c.fetchall()
        if rows[0][0] == None:
            # one empty json for each hour of the day, messages are stored as follows: peak[military_hour][message_id] = date
            peak = [{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}]
            # one empty json for each hour of the date, user entries are stored as follows: avgusers[military_hour][user_id] = date
            avgusers = [{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{}]
            # one date per array indices
            restarts = []

            c.execute("INSERT INTO analytics(ID, PEAK, AVGUSERS, RESTARTS) SELECT ?, ?, ?, ? WHERE NOT EXISTS(SELECT 1 FROM analytics WHERE ID = ?)", (1, json.dumps(peak), json.dumps(avgusers), json.dumps(restarts), 1))
            conn.commit()

        # Check to see if lastactive has been populated yet.
        c.execute("SELECT MAX(ID) FROM lastactive")
        rows = c.fetchall()
        if rows[0][0] == None:
            # simple json referenced with lastactive["user_id"] = {name:'Firstname Lastname', @: '@name', date: date}
            lastactive = {}
            c.execute("INSERT INTO lastactive(ID, LASTACTIVE) SELECT ?, ? WHERE NOT EXISTS(SELECT 1 FROM lastactive WHERE ID = ?)", (1, json.dumps(lastactive), 1))
            conn.commit()
        
        conn.close()

    def retrieveLastActive(self):
        self.log(DEBUG, "retrieveLastActive()")
        conn = sqlite3.connect(self.config.server['DB'])
        c = conn.cursor()
        c.execute("SELECT * FROM lastactive WHERE ID = 1")
        lastactive = c.fetchall()
        lastactive = lastactive[0]
        return lastactive

    def recordLastActive(self, data):
        self.log(DEBUG, "recordLastActive({})".format(data))
        lastactive = json.loads(self.retrieveLastActive()[1])
        
        if data["user_id"] not in lastactive:
            lastactive[data["user_id"]] = {}
        lastactive[data["user_id"]]["name"] = data["name"]
        lastactive[data["user_id"]]["@"] = data["@"]
        lastactive[data["user_id"]]["date"] = data["date"]

        conn = sqlite3.connect(self.config.server['DB'])
        c = conn.cursor()
        c.execute("UPDATE lastactive SET LASTACTIVE = ? WHERE ID = 1", (json.dumps(lastactive),))
        conn.commit()
        conn.close()

    def removeLAMember(self, user_id):
        self.log(DEBUG, "removeLAMember({})".format(user_id))
        lastactive = json.loads(self.retrieveLastActive()[1])

        if id in lastactive:
            del lastactive[user_id]
        conn = sqlite3.connect(self.config.server['DB'])
        c = conn.cursor()
        c.execute("UPDATE lastactive SET LASTACTIVE = ? WHERE ID = 1", (json.dumps(lastactive),))
        conn.commit()
        conn.close()
        
    def recordAnalytics(self, data):
        self.log(DEBUG, "recordAnalytics({})".format(data))
        conn = sqlite3.connect(self.config.server['DB'])
        c = conn.cursor()
        if "peak" in data:
            self.log(DEBUG, "DUMPING PEAK: {}".format(data['peak']))
            c.execute("UPDATE analytics SET PEAK = ? WHERE ID = ?;", (json.dumps(data['peak']), 1))
        if "avgusers" in data:
            self.log(DEBUG, "DUMPING AVGUSERS: {}".format(data['avgusers']))
            c.execute("UPDATE analytics SET AVGUSERS = ? WHERE ID = ?;", (json.dumps(data['avgusers']), 1))
        if "restarts" in data:
            self.log(DEBUG, "DUMPING RESTARTS".format(data['restarts']))
            c.execute("UPDATE analytics SET RESTARTS = ? WHERE ID = ?;", (json.dumps(data['restarts']), 1))
        conn.commit()
        conn.close()

    def recordRestart(self, date=None):
        if date == None:
            date = datetime.now()
        self.log(DEBUG, "recordRestart({})".format(date))
        data = json.loads(self.retrieveAnalytics()[3])
        data.append(date.strftime("%Y/%m/%d %H:%M:%S"))
        data = {"restarts": data}
        self.recordAnalytics(data)

    def retrieveAnalytics(self):
        self.log(DEBUG, "retrieveAnalytics")
        conn = sqlite3.connect(self.config.server['DB'])
        c = conn.cursor()
        c.execute("SELECT * FROM analytics")
        analytics = c.fetchall()
        analytics = analytics[0]
        return analytics

    # ###################################
    #  addToDB
    #
    #  Adds the message specified in
    #  message_json to the database.
    #  Message is added as follows
    #    ID, DATE, JSON
    #  If database is locked due to a
    #  current write being performed,
    #  then the function waits and tries
    #  again in .1s.
    # ###################################
    def addToDB(self, mType, message_json):
        self.log(DEBUG, "func --> addToDB")
        ID = message_json[mType]["message_id"]
        date = message_json[mType]["date"]
        while True:
            try:
                conn = sqlite3.connect(self.config.server["DB"])
                c = conn.cursor()
                c.execute("INSERT INTO messages(ID, DATE, JSON) SELECT ?, ?, ? WHERE NOT EXISTS(SELECT 1 FROM messages WHERE ID = ?)", (ID, date, json.dumps(message_json), ID))
                conn.commit()  # Save
                conn.close()
                return True
            except Exception as e:
                self.log(ERROR, "Exception Triggered by database lock. Trying again in .1s. Exception: {}".format(e))
                time.sleep(0.1)

    def addPollToDB(self, poll_json):
        self.log(INFO, "func --> addPollToDB")
        ID = poll_json['upid']
        while True:
            try:
                conn = sqlite3.connect(self.config.server["DB"])
                c = conn.cursor()
                c.execute("INSERT INTO polls(ID, JSON, CLOSED) SELECT ?, ?, ? WHERE NOT EXISTS(SELECT 1 FROM polls WHERE ID = ?)", (ID, json.dumps(poll_json), 0, ID))
                conn.commit()
                conn.close()
                return True
            except Exception as e:
                self.log(ERROR, "Exception Triggered by database lock. Trying again in .1s Exception: {}".format(e))
                time.sleep(0.1)

    def updatePollInDB(self, poll_json):
        self.log(INFO, "func --> updatePollInDB")
        ID = poll_json['upid']
        while True:
            try:
                conn = sqlite3.connect(self.config.server["DB"])
                c = conn.cursor()
                c.execute("UPDATE polls SET JSON = ? WHERE ID = ?;", (json.dumps(poll_json), ID))
                conn.commit()
                conn.close()
                return True
            except Exception as e:
                self.log(ERROR, "Exception triggered by database lock. Trying again in .1s Exception: {}".format(e))
                time.sleep(0.1)

    def getNewPollID(self):
        self.log(INFO, "func --> getMaxPollID")
        conn = sqlite3.connect(self.config.server["DB"])
        c = conn.cursor()
        c.execute("SELECT MAX(ID) FROM polls;")
        maxID = c.fetchall()
        conn.close()
        return maxID[0][0] + 1

    def closePoll(self, upid):
        self.log(INFO, "func --> closePoll")
        while True:
            try:
                conn = sqlite3.connect(self.config.server["DB"])
                c = conn.cursor()
                c.execute("UPDATE polls SET CLOSED = ? WHERE ID = ?;", (1,upid))
                conn.commit()
                conn.close()
                return True
            except Exception as e:
                self.log(ERROR, "Exception triggered by database lock. Trying again in .1s Exception: {}".format(e))
                time.sleep(0.1)

    # ###################################
    #  removeFromDB
    #
    #  Removes the message with the
    #  specified ID from the DB.
    # ###################################
    def removeFromDB(self, ID):
        self.log(DEBUG, "func --> removeFromDB")
        conn = sqlite3.connect(self.config.server["DB"])
        c = conn.cursor()
        c.execute("DELETE FROM messages WHERE ID = ?", (ID,))
        conn.commit()
        conn.close()

    # ###################################
    #  loadDB
    #
    #  Grabs all the message from the DB
    #  ordered by the ID, which is
    #  sequential based on the age of the
    #  messages.
    # ###################################
    def loadDB(self):
        self.log(DEBUG, "func --> loadDB")
        conn = sqlite3.connect(self.config.server["DB"])
        c = conn.cursor()
        c.execute("SELECT * FROM messages order by ID")
        messages = c.fetchall()
        # pp(messages)
        messages = sorted(messages, key=lambda message: int(message[1]))
        conn.close()
        return messages

    def loadPollsFromDB(self):
        self.log(INFO, "func --> loadPollsFromDB")
        conn = sqlite3.connect(self.config.server["DB"])
        c = conn.cursor()
        c.execute("SELECT * FROM polls order by ID WHERE CLOSED = 0;")
        polls = c.fetchall()
        conn.close()
        return polls
