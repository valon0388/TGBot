#!/usr/bin/env python3

from logger import *
from config import Config
from SMSProcessor import SMSProcessor

from twilio.rest import Client
from http.server import HTTPServer
from http.server import BaseHTTPRequestHandler
import ssl
import _thread
import time
import os
import urllib.parse
from urllib.parse import urlparse
import json



class SMSRequestHandler(BaseHTTPRequestHandler):
    def __init__(self, *args, **kwargs):
        print("Started Request Thread")
        self.processor = SMSProcessor(configfile)
        self.config = Config(configfile)
        self.logger = Logger()
        super().__init__(*args, **kwargs)

    def log(self, level, statement):
        self.logger.log(level, "SMSRequestHandler -- {}".format(statement))

    def process_post_data(self, post_data):
        self.log(DEBUG, "process_post_data")
        self.log(INFO, "MESSAGE: {}".format(post_data))
        self.processor.process_post_data(post_data)

    def set_success_headers(self):
        self.log(DEBUG, "set_success_headers")
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def set_forbidden_headers(self):
        self.log(DEBUG, "set_forbidden_headers")
        self.send_response(403)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self.log(DEBUG, "do_GET")
        self.set_forbidden_headers()
        self.send_response(403)

    def do_POST(self):
        self.log(DEBUG, "do_POST")
        try:
            content_length = int(self.headers['Content-Length'])  # gets size
            self.log(DEBUG, "CONTENT_LENGTH: {}".format(content_length))
            post_data_orig = self.rfile.read(content_length)
            self.log(DEBUG, "POST_DATA: {}".format(post_data_orig))
            post_data_json = urllib.parse.parse_qs(post_data_orig.decode("utf-8"))
            self.log(DEBUG, "MESSAGE JSON: {}".format(post_data_json))
            self.log(DEBUG, "Self Path: {}, Twiliotoken: {}".format(self.path, self.config.server['TWILIOTOKEN']))
            if self.path[1:] == self.config.server["TWILIOTOKEN"] and post_data_json['From'][0] == self.config.server['AUTHNUM']:
                self.log(DEBUG, "PATH: {}  and AUTHNUM: {} Match! Sending message to processing...".format(self.path, self.config.server['AUTHNUM']))
                _thread.start_new_thread(self.process_post_data, (post_data_orig,))
                self.set_success_headers()
                response = """
                        <?xml version="1.0" encoding="UTF-8"?>
                        <Response>
                            <Message>{0}</Message>
                        </Response>
                        """
                self.wfile.write(response.format(self.config.server["MSG_ACCEPT"]).encode())
            else:
                self.set_forbidden_headers()
                self.wfile.write(
                    b"<html><body><h1>403 FORBIDDEN</h1></body></html>"
                    )
        except Exception as e:
            self.log(ERROR, "!!!!!!!!!!!!!!!!! Exception!!!!!!!!!!!!!!!!!!!: {}".format(e))

def push_listener(threadName, delay):
    httpd.serve_forever()

def sendSMS(body):
    client = Client(account_sid, auth_token) 
 
    message = client.messages.create(from_=phNumber,body=body,to=myNumber) 
 
    print(message.sid)


if __name__ == '__main__':
    print("Starting SMS Listener")
    global httpd
    global configfile
    global mynum
    global authnum
    global account_sid
    global auth_token
    configfile = sys.argv[1]
    config = Config(configfile, sms=True)
    logger = Logger(auto=True, logname=config.server['LOGNAME'])
    logger.setup(config.server['LOGNAME'])
    logger.log(DEBUG, "LIVE: {}".format(config.server['LIVE']))
    dirname = os.path.expanduser('~')
    pub = os.path.join(dirname, config.config['server']['cert'])
    key = os.path.join(dirname, config.config['server']['key'])
    logger.log(DEBUG, "main -- PUB: {} KEY: {}".format(pub, key))

    mynum = config.server['MYNUM']
    authnum = config.server['AUTHNUM']
    account_sid = config.server['TWILIOSID']
    auth_token = config.server['TWILIOTOKEN']
    
    context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    context.load_cert_chain(pub, key)
    context.options |= ssl.OP_NO_TLSv1 | ssl.OP_NO_TLSv1_1
    context.set_ciphers('EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH')
    server_address = (config.server['IP'], int(config.server['PORT']))
    httpd = HTTPServer(server_address, SMSRequestHandler)
    httpd.socket = context.wrap_socket(httpd.socket, server_side=True)
    _thread.start_new_thread(push_listener, ("smsHandler", 0))

    while True:
        time.sleep(60)


