from logger import *
from config import Config
from InputProcessor import InputProcessor

from flask import Flask, make_response, request, Response
import _thread

bot = Flask(__name__)

class BRHandler:
    
    def __init__(self):
        self.logger = Logger()
        self.processor = InputProcessor()
        self.config = Config()
        self.bot = bot

    def get_bot(self):
        return self.bot
   
def log(level, statement):
    logger = Logger()
    logger.log(level, "botRequestHandler: {}".format(statement))

@bot.route('/<token>', methods=['POST'])
def handle_request(token):
    log(DEBUG, "func --> handle_request({})".format(token))
    config = Config()
    if token == config.telegram['TOKEN']:
        post_data = request.get_data()
        log(DEBUG, "NEW MESSAGE AFTER READ: type:{} {}".format(type(post_data), post_data))
        processor = InputProcessor()
        _thread.start_new_thread(processor.process_post_data, (post_data,))
        return "<html><body><h1>POST!</h1></body></html>\n", 200
    else:
        return "<html><body><h1>403 FORBIDDEN</h1></body></html>", 403
