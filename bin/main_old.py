#!/usr/bin/env python3
# v1.01

# This file is part of TGBOT.

# TGBOT is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# TGBOT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with TGBOT.  If not, see <http://www.gnu.org/licenses/>.

# Local imports
# Imports Logger class as well as predefined Logging levels(INFO, DEBUG, ERROR)
from logger import LOGINFO,LOGDEBUG,LOGWARN,LOGERROR
from logger import INFO,DEBUG,WARN,ERROR,CRIT
from logger import Logger
from config import Config
from BRHandler import botRequestHandler
from MessageQueue import MessageQueue
from InputProcessor import InputProcessor
from botcalendar import BotCalendar
from TGInterface import TGInterface

#from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler
from socketserver import ThreadingMixIn
from http.server import HTTPServer,BaseHTTPRequestHandler
import ssl
import json
import time
import _thread
import sys
import os

config = ''
DB = ''
MQ = ''
CAL = ''
TGI = ''
#POLLER = ''


logger = ''
httpd = ''


# ###################################
#  Log
#
#  Local log method to specify the
#  name of the class/file of the
#  caller.
# ###################################
def log(level, statement):
    logger.log(level, "main -- {}".format(statement))

#class ThreadedHTTPServer(ThreadingMixIn,HTTPServer):
#    "threads connections"

# ###################################
#  __init__
#
#  Initializes the TGInterface, Calendar
#  MessageQueue, and the botRequestHandler.
#  Starts the botRequestHandler and wraps
#  the socket in an SSL context.
# ###################################
def __init__(CONFIGFILE):
    global config
    global DB
    global MQ
    global CAL
    global TGI
    global logger
#    global POLLER

    print("CONFIG FILE: {}".format(CONFIGFILE))
    config = Config(CONFIGFILE)
    logger = Logger(auto=True, logname=config.server['LOGNAME'])
    logger.setup(config.server['LOGNAME'])
    log(DEBUG, "func --> __init__")
    TGI = TGInterface()
    CAL = BotCalendar()
    MQ = MessageQueue()
    MQ.loadDB()
#    POLLER = Poller()
#    POLLER.loadPolls()

    print("CERT: {} KEY: {}".format("{}/{}".format(os.getcwd(), config.server['CERT']), config.server['KEY']))
    context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    context.load_cert_chain(config.server['CERT'], config.server['KEY'])# 1. key, 2. cert, 3. intermediates
    context.options |= ssl.OP_NO_TLSv1 | ssl.OP_NO_TLSv1_1  # optional
    context.set_ciphers('EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH')

    log(INFO, "Attempting to bind to IP: {} and port: {}".format(config.server["IP"], int(config.server["PORT"])))
    server_address = (config.server["IP"], int(config.server["PORT"]))
    global httpd
    #httpd = ThreadedHTTPServer(server_address, botRequestHandler)
    httpd = HTTPServer(server_address, botRequestHandler)
    httpd.socket = context.wrap_socket(httpd.socket, server_side=True)


# ###################################
#  push_listener
#
#  Starts the botRequestHandler listener
#  for the message pushes from the Telegram
#  api.
# ###################################
def push_listener(threadName, delay):
    log(INFO, "Starting the push_listener thread: {}".format(threadName))
    httpd.serve_forever()


# ###################################
#  calendar_checker
#
#  Starts the loop that triggers the
#  calendar check once every 24 hours.
# ###################################
def calendar_checker(threadName, delay):
    log(INFO, "Starting the calendar_checker thread: {}".format(threadName))
    while True:
        time.sleep(86400)  # 24 hours
        CAL.check(auto=True)

#def polldate_checker(threadName, delay):
#    log(INFO, "Starting the polldate_checker thread: {}".format(threadName))
#    while True:
#        time.sleep(60)



  ###################################
#  Starts the main threads for the
#  botRequestHandler and the Calendar.
# ###################################
def startbot(CONFIGFILE):
     __init__(CONFIGFILE)
     _thread.start_new_thread(push_listener, (("push_listener", 0)))
     _thread.start_new_thread(calendar_checker, (("calendar_checker", 0)))

     MQ.addBotMessage(config.group_info["STARTUP"])
     CAL.build_eventQueue()

     log(INFO, "AFTER PUSH LISTENER")

     # ###################################
     #  Triggers the processing of the message queue
     #  and the calendar check for urgent events
     #  once every minute.
     # ###################################
     while True:
          MQ.processQueue()
          CAL.get_urgentEvents()
     #    POLLER.closeChecker()
          time.sleep(60)




if __name__ == "__main__":
    startbot(sys.argv[1])
